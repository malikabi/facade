<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use JWTAuth;
use Auth;
use DB;

// traits
use App\Http\Controllers\Traits\StoreTrait;
use App\Http\Controllers\Traits\ListingTrait;

class MainController extends Controller
{
    use StoreTrait, ListingTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function store()
    {
        try {
            // start db transaction
            DB::beginTransaction();

            if($this->storeTrait($this->model)){
                
                DB::commit();
                return $this->success(DATA_ADDED_SUCCESSFUL);

            }else return $this->error(GENERAL_FAILURE_MESSAGE, ERROR_400);
            
        } catch (QueryException $e) {
            DB::rollback();
            return $this->error($e->getMessage(), ERROR_500);
        } catch (Exception $e) {
            DB::rollback();
            return $this->error($e->getMessage(), ERROR_500);
        }
        
    }

    public function listing()
    {
        try {
            
            return $this->successWithData(DATA_ADDED_SUCCESSFUL, $this->listingTrait($this->model, $this->with));
            
        } catch (QueryException $e) {
            DB::rollback();
            return $this->error($e->getMessage(), ERROR_500);
        } catch (Exception $e) {
            DB::rollback();
            return $this->error($e->getMessage(), ERROR_500);
        }
        
    }

}
