<?php


namespace App\Http\Controllers\Traits;

/**
 * Description of ActivityLog
 *
 * @author Muhammad Abid
 */
trait ListingTrait
{
    
    /**
     * Description: The following method sotre all database action into activity_logs table
     * @author Muhammad Abid - I2L
     * @param $model
     * @param $action
     * @return array
     */
    public function listingTrait($model, $with = [])
    {
        $query = $model->newQuery();

        $query->with($with);

        if(!empty(request('search'))){

            foreach($model->getFillable() AS $key => $column){
                $query->orWhere($column, 'LIKE', '%'.request('search').'%');
            }

        }


        return $query->paginate(DEFAULT_PER_PAGE);
    }
}
