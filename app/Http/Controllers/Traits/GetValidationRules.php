<?php


namespace App\Http\Controllers\Traits;

/**
 * Description of ActivityLog
 *
 * @author Muhammad Abid
 */
trait GetValidationRules
{
    
    /**
     * Description: The following method sotre all database action into activity_logs table
     * @author Muhammad Abid - I2L
     * @param $model
     * @param $action
     * @return array
     */
    public function getValidationRules()
    {
        $requestClass = parseRoute('Request');
        return new $requestClass;
    }
}
