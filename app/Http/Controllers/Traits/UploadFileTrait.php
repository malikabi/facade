<?php


namespace App\Http\Controllers\Traits;

/**
 * Description of ActivityLog
 *
 * @author Muhammad Abid
 */
trait UploadFileTrait
{
    
    /**
     * Description: The following method sotre all database action into activity_logs table
     * @author Muhammad Abid - I2L
     * @param $model
     * @param $action
     * @return array
     */
    public function uploadFileTrait($model)
    {
        $this->deleteFile($model);

        // //get file path
        // $filePath = $this->filePath;
        // //Set public folder path
        // $folderPath = env('ASSET_URL') .'/'. $filePath;
        // //renaming the file
        // $name = time() . '_' . rand(5000, 100000) . "." . explode('/', $mimeType)[1];

        // // check the file is image or not
        // if(strpos(explode('/', $mimeType)[1], 'pdf') !== false){
        //     file_put_contents($folderPath . $name, base64_decode($imageBytes));
        // }else{
        //     Image::make(base64_decode($imageBytes))->save($folderPath . $name, 60);
        // }
        

        // //update permission
        // chmod($folderPath . $name, 0777);

        // // $image = 

        // // $model->base64 = $this->getBase64($folderPath . $name);
        // $model->{$this->imageName} = $filePath . $name;

        // return $model;
    }

    public function deleteFile($model)
    {
        if(is_file($model->{$model->imageName})){
            unlink($model->{$model->imageName});
        }
    }
}
