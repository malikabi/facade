<?php


namespace App\Http\Controllers\Traits;

use App\Http\Controllers\Traits\UploadFileTrait;

/**
 * Description of ActivityLog
 *
 * @author Muhammad Abid
 */
trait StoreTrait
{
    use UploadFileTrait;
    
    /**
     * Description: The following method sotre all database action into activity_logs table
     * @author Muhammad Abid - I2L
     * @param $model
     * @param $action
     * @return array
     */
    public function storeTrait($model)
    {
        $model = $model->newInstance();

        $model = $model->fill(request()->all());

        if($model->save()){

            if(isset(request()->{$model->imageName})){
                $this->uploadFileTrait($model);
            }
            
            if($model->custom()){
                return true;
            }else return false;
        }else return false;
    }
}
