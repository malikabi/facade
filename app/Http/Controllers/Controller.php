<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

// traits
use App\Http\Controllers\Traits\InitModel;
use App\Http\Controllers\Traits\GetValidationRules;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, InitModel, GetValidationRules;


     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->request = request();
        $this->init();
    }

    public function init()
    {
        $this->model = $this->initModel();
        $this->validateRequest();
        $this->with = $this->getValidationRules()->with();
    }

    public function validateRequest()
    {
        $validator = $this->getValidationFactory()->make(
            $this->request->all(),
            $this->getValidationRules()->rules()
        );
        if($validator->fails()){
            return $this->faildValidation($validator);
        }
    }

    public function faildValidation($validator)
    {
        $json = [
            'success' => false,
            'message' => $validator->errors()->all(),
        ];
        $response = new JsonResponse( $json, 400 );
        throw (new ValidationException($validator, $response))->status(400);
    }

    
    public function error($message, $code)
    {
        return response()->json([
            'success' => false,
            'message' => $message
        ], $code);
    }
    
    public function success($message)
    {
        return response()->json([
            'success' => true,
            'message' => $message
        ], SUCCESS_200);
    }

    public function successWithData($message, $data)
    {
        return response()->json([
            'success' => true,
            'message' => $message,
            'data' => $data
        ], SUCCESS_200);
    }


}
