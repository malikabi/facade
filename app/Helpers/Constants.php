<?php

// error codes
define('ERROR_400', 400);
define('ERROR_403', 403);
define('ERROR_401', 401);
define('ERROR_500', 500);
// success codes
define('SUCCESS_200', 200);


// success Messages
define('DATA_FETCH_SUCCESSFUL', 'Data fetched successfully');
define('DATA_ADDED_SUCCESSFUL', 'Details added successfully');
define('DATA_DELETED_SUCCESSFUL', 'Details deleted successfully');
define('DATA_UPDATED_SUCCESSFUL', 'Details updated successfully');
// general failer

define('GENERAL_FAILURE_MESSAGE', 'Operation failed');
define('PERMISSION_DENIED', 'Permission Denied');


// pagination
define('DEFAULT_PER_PAGE', 10);

