<?php

if (!function_exists('parse_route')) {

    /**
     * Description: The following method is used to generate the unique business reference ID string
     * @author Muhammad Abid
     * @param modelName
     * @return string
     */
    function parseRoute($className)
    {
        switch ($className) {
            case 'Model':
                return 'App\\' . studly_case(ucfirst(head(explode('/', last(explode('admin/', request()->getPathInfo()))))));
                break;
            case 'Request':
                return 'App\Http\Requests\\' . studly_case(ucfirst(head(explode('/', last(explode('admin/', request()->getPathInfo())))))) .'\\'. studly_case(ucfirst(last(explode('/', request()->getPathInfo())))) . 'Request';
                break;
        }
    }
}