<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'user_id'
    ];

    
    public function store()
    {
        $model = $this->newInstance();

        $model = $model->fill(request()->all());

        if($model->save()){
            if($model->custom()){
                return true;
            }else return false;
        }else return false;
    }
    public function custom()
    {
        return true;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
