<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Auth', 'prefix' => 'admin/auth'], function () {
    Route::post('login', 'LoginController@login');
    Route::post('reset-password', 'LoginController@resetPassword');
});

//=============================//
//======= Private Area ========//
//=============================//
Route::group(['prefix' => 'admin', 'middleware' => 'UserJWTAuth'], function () {

    Route::group(['prefix' => 'user'], function () {

        Route::post('store', 'MainController@store');
        Route::get('listing', 'MainController@listing');

    });
    Route::group(['prefix' => 'post'], function () {

        Route::post('store', 'MainController@store');
        Route::get('listing', 'MainController@listing');

    });
    


});
